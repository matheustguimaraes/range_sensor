 library ieee;
use ieee.std_logic_1164.all;
 
entity trigger_generator is 
	port(
		clk: in std_logic;
		trigger: out std_logic
	);
end trigger_generator;
 
architecture behavioral of trigger_generator is
 
component counter is
	generic(
		n: POSITIVE := 10
	);
	port(
		clk: in std_logic;
		enable: in std_logic;
		reset: in std_logic; -- Active low
		count_out: out std_logic_vector(n - 1 downto 0)
	);
end component;
 
signal resetCounter: std_logic;
signal outputCounter: std_logic_vector(23 downto 0);
 
begin  
	trigg : counter generic map (24) port map (clk, '1', resetCounter, outputCounter);
	
	process(clk, outputCounter)
  
	constant ms250: std_logic_vector(23 downto 0) := "101111101011110000100000"; -- 12500000 ms
 	constant ms250and100us: std_logic_vector(23 downto 0) := "101111101100000000001000";-- 12501000 ms "101111101100111110101000";
	begin

		if(outputCounter > ms250 and outputCounter < ms250and100us) then
			trigger <= '1';
		else
			trigger <= '0';
		end if;
    
		if(outputCounter = ms250and100us or outputCounter= "XXXXXXXXXXXXXXXXXXXXXXXX") then
			resetCounter <= '0';
		else
			resetCounter <= '1';
		end if;
  
	end process;
end behavioral;