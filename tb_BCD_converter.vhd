library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_BCD_converter is
end entity;

architecture behavior of tb_BCD_converter is
  signal dist : std_logic_vector(8 downto 0);
  signal tb_bcd2, tb_bcd1, tb_bcd0 : std_logic_vector(3 downto 0);

begin

  uut: entity work.BCD_converter port map( 	Distance => dist,
						hundreds => tb_bcd2,
						tens => tb_bcd1,
						unit => tb_bcd0);

  

dist <= "010100100"; -- 36, 

end architecture;

